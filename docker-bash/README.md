# DevOps Challenge

Docker Bash

## System requirements for development

- CentOS 7
- Docker-CE
- HaProxy 1.6.14
- Nginx
- Git

## Run create docker haproxy + nginx

```bash

## Run as root or sudo
## Script install docker-ce, git, docker-compose on CentOS 7

cd /
yum update -y && yum install -y git sudo
git clone https://gitlab.com/jniltinho/devops-challenge.git
cd devops-challenge/docker-bash
bash create-docker-haproxy.sh

## For Nginx1
## Run http://centos_ip:80
## Run http://centos_ip:80/haproxy?stats

## For Nginx2
## Run http://centos_ip:81
## Run http://centos_ip:81/haproxy?stats
```