#!/bin/bash
## Install Docker-CE on CentOS 7 64Bits
## Author: Nilton OS <jniltinho@gmail.com> | https://www.linuxpro.com.br
## Version: 0.2
## CentOS ISO: http://mirror.ufscar.br/centos/7.6.1810/isos/x86_64/CentOS-7-x86_64-Minimal-1810.iso

## Install Docker-CE on CentOS 7 64Bits
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce
sudo usermod -aG docker $(whoami)
sudo systemctl enable docker.service
sudo systemctl start docker.service


## Install Docker Compose
sudo yum install -y epel-release
sudo yum install -y python-pip
sudo pip install --upgrade pip
sudo pip install docker-compose


## Create HAProxy + Nginx
docker build -t haproxy .

## Create Network for Haproxy + Nginx
docker network create -d bridge ha_ng

## Run Containers
docker run -p 80:5000 --network ha_ng -d --name nginx1 haproxy
docker run -p 81:5000 --network ha_ng -d --name nginx2 haproxy
docker rm -f nginx1
docker run -p 80:5000 --network ha_ng -d --name nginx1 haproxy


## List Docker Images, Network and Status Run
docker images
docker network ls
docker ps
