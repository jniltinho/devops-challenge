FROM centos:7
LABEL maintainer="Nilton OS <jniltinho@gmail.com>"


# Compile HAProxy
# https://github.com/haproxy/haproxy/blob/master/README
# https://gist.github.com/meanevo/742b61031fdf9ce01e50d4b196a3f31e

# docker build -f build-haproxy.Dockerfile -t haproxy .
# docker build -f build-nginx.Dockerfile -t nginx .
# docker network create -d bridge ha_ng
# docker run --network ha_ng -d --name nginx1 nginx
# docker run --network ha_ng -d --name nginx2 nginx
# docker run --network ha_ng -d -p 80:5000 --name haproxy haproxy
# docker run --rm -it haproxy /bin/bash
# docker run --rm -it nginx /bin/bash

# docker stop $(docker ps -a -q)
# docker rm -f $(docker ps -a -q)
# docker rmi -f $(docker images -q)

# update, install required, clean
RUN yum install -y epel-release \ 
    && yum install -y curl gcc pcre-static pcre-devel openssl-devel make \
    && yum clean all \
    && curl -LksO https://www.haproxy.org/download/1.6/src/haproxy-1.6.14.tar.gz \
    && tar xzvf haproxy-1.6.14.tar.gz \
    && cd haproxy-1.6.14 \
    && make TARGET=linux2628 \
    && make install \
    && mkdir -p /etc/haproxy /var/lib/haproxy \
    && touch /var/lib/haproxy/stats \
    && ln -s /usr/local/sbin/haproxy /usr/sbin/haproxy \
    && cd / && rm -rf haproxy-1.6.14* \
    && useradd -r haproxy



# setup haproxy configuration
ADD ./etc/haproxy.cfg /etc/haproxy/haproxy.cfg

# Expose ports
EXPOSE 5000

CMD ["/usr/sbin/haproxy", "-f", "/etc/haproxy/haproxy.cfg"]