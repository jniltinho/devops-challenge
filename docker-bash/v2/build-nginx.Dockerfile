FROM centos:7
LABEL maintainer="Nilton OS <jniltinho@gmail.com>"

# docker build -f build-haproxy.Dockerfile -t haproxy .
# docker build -f build-nginx.Dockerfile -t nginx .
# docker network create -d bridge ha_ng
# docker run --network ha_ng -d --name nginx1 nginx
# docker run --network ha_ng -d --name nginx2 nginx
# docker run --network ha_ng -d -p 80:5000 --name haproxy haproxy
# docker run --rm -it haproxy /bin/bash
# docker run --rm -it nginx /bin/bash

# docker stop $(docker ps -a -q)
# docker rm -f $(docker ps -a -q)
# docker rmi -f $(docker images -q)

# update, install required, clean
RUN yum install -y epel-release \ 
    && yum install -y nginx \
    && yum clean all


# setup haproxy configuration
ADD ./etc/nginx.conf /etc/nginx/nginx.conf

# Expose ports
EXPOSE 80


CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
