#!/bin/bash
## Create HAProxy + Nginx and RUN
## Author: Nilton OS <jniltinho@gmail.com> | https://www.linuxpro.com.br
## Version: 0.1


## Create HAProxy + Nginx and RUN
docker build -f build-haproxy.Dockerfile -t haproxy .
docker build -f build-nginx.Dockerfile -t nginx .
docker network create -d bridge ha_ng
docker run --network ha_ng -d --name nginx1 nginx
docker run --network ha_ng -d --name nginx2 nginx
docker run --network ha_ng -d -p 80:5000 --name haproxy haproxy