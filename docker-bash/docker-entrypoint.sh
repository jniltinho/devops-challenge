#!/bin/bash
#set -Eeo pipefail

/usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg -D


exec "$@"